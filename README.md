# coder-desafio-38

`http://localhost:8080/ingreso`

## capas:

el proyecto tiene las siguientes capas principales:

[routes]: paths y endpoints disponibles

[controllers]: orquesta los request y responses, también se ocupa de renderizar los html para este proyecto de tipo server side rendering

[services]: procesa y tiene la lógica de la funcionalidad, se conecta con los dao

[dao]: secontiene la conexión a la base de datos como los modelos, es el que se comunica con la base.

## Estructura del proyecto:

```
.
├── imgs
├── logs
├── performance
│   └── report
├── src
│   ├── auth
│   ├── config
│   ├── controllers
│   ├── dao
│   │   ├── db
│   │   └── models
│   ├── graphql
│   │   ├── resolvers
│   │   └── schema
│   ├── resources
│   ├── routes
│   ├── services
│   ├── utils
│   └── views
│       ├── layouts
│       └── partials
└── target
```

## Inicio del server

nodemon

npm start

## graphql

[![Watch the video](https://res.cloudinary.com/hdsqazxtw/image/upload/v1570710978/coderhouse.jpg)](https://youtu.be/bP6-uUih_QE)

### url:

`http://localhost:8080/graphql`

### implementación

```
│   ├── graphql
│   │   ├── resolvers
│   │   └── schema
```

1. `src/graphql/schema/index.js`schema define las entidades y los métodos de graphql.
2. `src/graphql/resolvers/index.js` contiene el mapeo entre el schema y los métodos de node. acá hay un 3er método qeu trae un producto dado un id, no está actualmente implementado en el front.
3. `src/server.js` - se pasa el objeto de graphql como middleware de express con la referencia al schema y resolvers.
4. `src/resources/list-productos-graphql.js` contiene el fetch con la query de graphql que se trae el listado de producto . `[GET]`
5. `src/resources/input-productos.js` contiene el fetch que postea via graphql un nuevo producto. `[POST]`
